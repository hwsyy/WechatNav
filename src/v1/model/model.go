package model

type PublishForm struct {
	AppType      string `form:"app.type" binding:"required"`
	AppName      string `form:"app.name" binding:"required"`
	AppUrl       string `form:"app.url" binding:"required"`
	AppIco       string `form:"app.ico" binding:"required"`
	AppErcode    string `form:"app.ercode" binding:"required"`
	AppPic1      string `form:"app.pic1" binding:"required"`
	AppPic2      string `form:"app.pic2"`
	AppPic3      string `form:"app.pic3"`
	AppPic4      string `form:"app.pic4"`
	AppPic5      string `form:"app.pic5"`
	AppTags      string `form:"app.tags"`
	AppIntroduce string `form:"app.introduce" binding:"required"`
	CreateDate   string
	Status       string
	Recommend    string
	View         string
}
