package main

import (
	"v1/controller"
	"os"

	"runtime"
)

func main() {
	runtime.GOMAXPROCS(4)
	port:=os.Args[1]
	//log.Infof("port: %s",port)
	controller.Start(port)


}
